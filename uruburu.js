(function (name, global, definition) {
	if (typeof module !== 'undefined') {
		var d3 = require('../d3/d3');
		module.exports = definition(d3, name, global);
	} else if (typeof define === 'function' && typeof define.amd  === 'object') {
		define(['../d3/d3'], definition);
	} else {
		global[name] = definition(global.d3, name, global);
	}
})('uruburu', this, function (d3, name, global) {
	if (global && global.uruburu) return global.uruburu;
	if (window.uruburu) return window.uruburu;
	var cos = Math.cos;
	var sin = Math.sin;
	var atan2 = Math.atan2;
	var π = Math.PI;

	var EventEmitter = function() {
		this._listeners = {};
	}

	EventEmitter.prototype.on = function(name, listener) {
		if (!this._listeners[name]) {
			this._listeners[name] = [ listener ];
		} else {
			this._listeners[name].push(listener);
		}
		return this;
	}

	EventEmitter.prototype.off = function(name, listener) {
		if (fn && this._listeners[name]) {
			this._listeners[name].splice(this._listeners[name].indexOf(listener), 1);
		}
	}

	EventEmitter.prototype.emit = function(name) {
		var listeners = this._listeners[name] || [],
			args = Array.prototype.slice.call(arguments, 1);
		for(var i = 0, len = listeners.length; i < len; ++i) {
			listeners[i].apply(this, args);
		}
	}

	var uruburu = new EventEmitter();

	uruburu.randStr = function() {
		return Math.random().toString(36).slice(2);
	}

	uruburu.s_rad2deg = function(a) {
		return a*180/π;
	}	
	
	uruburu.s_angle = function(x, y) {
		return Math.atan2(y, x);
	}

	var callbacks = {};
	uruburu.registerCallback = function(name, cb) {
		callbacks[name] = cb;
	}
	uruburu.unregisterCallback = function(name, cb) {
		if (callbacks.hasOwnProprerty(name)) {
			delete callbacks[name];
		}
	}







	// PieGauge
	uruburu.PieGauge = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'min', 'max', 'color', 'x', 'y', 'r', 'accuracy', 'percentage', 'math'];
		self.type = 'PieGauge';
		
		var _name = options.name || 'pie_gauge_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				self.redraw();
			} else {
				return _name;
			}
		}

		var _innerPadding = 30;
		
		self.htmlElement = container.append('div')
			.attr('id', _name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');		
		
		var _min = options.min ? options.min : 0;
		self.min = function(val) {	
			if (val !== undefined) {
				_min = val;
				self.redraw();
			} else {
				return _min;
			}	
		}

		var _max = options.max ? options.max : 100;
		self.max = function(val) {		
			if (val !== undefined) {
				_max = val;
				self.redraw();
			} else {
				return _max;
			}
		}
		
		var _color = options.color ? options.color : '#ff0000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}
		
		var _r = options.r ? options.r : 75;
		self.r = function(val) {
			if (val !== undefined) {
				_r = val;
				self.redraw();
			} else {
				return _r;
			}
		}	
				
		self.width = function() { return 2*self.r(); };
		self.height = function() { return 2*self.r(); };

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
			
		//console.log(eval("function comp(val) { return (val*3600/100).toFixed(0) + 'kmph'; }"))	
		var _math = options.math ? Function("val", 'return ' + options.math + ';') : undefined;
		self.math = function(val) {
			if (val !== undefined) {
				_math = eval(val);
				self.redraw();
			} else {
				return _math;
			}			
		} 

		var _percentage = options.percentage ? options.percentage : false; 
		self.percentage = function(val) { 
			if (val !== undefined) {
				_percentage = val;
				self.redraw();
			} else {
				return _percentage;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				if (!gauge || !svgtxt) return;
				if (self.min() < 0) {
					if (val < 0) {
						data = [ self.max()/range, 
								 -val/range, 
								 (-self.min()+val)/range ];
					} else {
						data = [ (self.max()-val)/range, 
								 val/range, 
								 -self.min()/range ];
					}
				} else {
					data = [ self.min()/range, 
							 (self.max()-val-self.min())/range, 
							 (val-self.min())/range ];
				}
				gauge = gauge.data(donut(data)); // update the data
				gauge.attr('d', arcgen); // redraw the arcs
				if (self.percentage()) {
					svgtxt.text((val/range*100).toFixed(self.accuracy()) + '%');
				} else {
					svgtxt.text(val.toFixed(self.accuracy()));
				}
				if (self.math()) {
					svgtxt_sec.text(self.math()(val));
				}
			} else {
				return _value;
			}
		}
		
		var range, data, donut, colors, arcgen;
		var gauge, svgroot, svggroup, svgtxt, svgtxt_sec, svgNametxt;
		
		self.draw = function() {
			range = self.max() - self.min();
			donut = d3.layout.pie().sort(null);
			
			if (self.min() < 0) {
				colors = ['#aaaaaa', self.color(), '#aaaaaa'];
				data = [ self.max()/range, 
						 self.value()/range,
						 (-self.min()-self.value())/range ];
			} else {
				colors = ['#aaaaaa', '#aaaaaa', self.color()];
				data = [(self.max()-self.value()+self.min())/range, 
						(self.value()-self.min())/range,  
						self.min()/range];
			}

			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', 2*self.r()+2*_innerPadding)
				.attr('height', 2*self.r()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');

			svgroot.append('svg:title').text('Name: ' + self.name());
			
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.r()+_innerPadding)+
					','+(self.r()+_innerPadding)+')');
			
			arcgen = d3.svg.arc().innerRadius(3*self.r()/4).outerRadius(self.r());
			
			gauge = svggroup.selectAll('path')
				.data(donut(data))
			  .enter().append('svg:path')
				.attr('x', self.x())
				.attr('y', self.y())
				.attr('fill', function(d, i) { return colors[i]; })
				.attr('d', arcgen)
				.each(function(d) { this._current = d; });
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', self.r()/3)
				.style('text-anchor', 'middle')
				.attr('dy', '0.25em')
				.text(0);
			
			if (self.math()) {
				svgtxt_sec = svggroup.append('svg:text')
					.style('font-family', 'DigitaldreamRegular')
					.style('font-size', self.r()/8)
					.style('text-anchor', 'middle')
					.attr('y', self.r()/3)
					.attr('dy', '0.25em')
					.text(0);
			}

			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'middle')
				.attr('y', self.r() + 20)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', 3*self.r()/4)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', self.r())
				.style('fill-opacity', 0)
				.style('stroke', '#333333');
			
			// min/max tick
			svggroup.append('svg:line')
				.attr('x1', 0)
				.attr('y1', -3*self.r()/4)
				.attr('x2', 0)
				.attr('y2', -self.r()-20)
				.style('stroke', '#333333');
		
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'start')
				.attr('x', 2)
				.attr('y', -self.r()-12)
				.text(self.max().toString());
			
			// min text
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'end')
				.attr('x', -2)
				.attr('y', -self.r()-2)
				.text(self.min().toString());
			
			if (self.min() < 0) {
				svggroup.append('svg:line')
					.attr('x1', 3/4*self.r()*cos(2*π+2*π*self.min()/range-π/2))
					.attr('y1', 3/4*self.r()*sin(2*π+2*π*self.min()/range-π/2))
					.attr('x2', (self.r()+10)*cos(2*π+2*π*self.min()/range-π/2))
					.attr('y2', (self.r()+10)*sin(2*π+2*π*self.min()/range-π/2))
					.style('stroke', '#333333');
				
				var svg0txt = svggroup.append('svg:text')
					.attr('x', 6)
					.attr('y', (self.r()+12)*sin(2*π+2*π*self.min()/range-π/2))
					.style('text-anchor', 'start') 
					.style('font-family', 'sans-serif')
					.style('font-size', '1em')
					.text(0);
				
				if (Math.abs(2*π+2*π*self.min()/range-π/2) > π/2) {
					svg0txt.style('text-anchor', 'end')
						.attr('x', (self.r()+12)*cos(2*π+2*π*self.min()/range-π/2) - 2)
						.attr('y', (self.r()+12)*sin(2*π+2*π*self.min()/range-π/2));
				} else if (Math.abs(2*π+2*π*self.min()/range-π/2) < π/2) {
					svg0txt.style('text-anchor', 'start')
						.attr('x', (self.r()+12)*cos(2*π+2*π*self.min()/range-π/2) + 2)
						.attr('y', (self.r()+12)*sin(2*π+2*π*self.min()/range-π/2));
				} else {
					svg0txt.style('text-anchor', 'middle');
				}
			}		
		}
		
		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}
	
		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();
		self.value(_value);
		self.x(_x);
		self.y(_y);
	}
	// end of PieGauge









	// PieGauge CASCO
	uruburu.PieGaugeCASCO = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'min', 'max', 'color', 'x', 'y', 'r', 'accuracy', 'percentage', 'math'];
		self.type = 'PieGaugeCASCO';
		
		var _name = options.name || 'pie_gauge_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				self.redraw();
			} else {
				return _name;
			}
		}

		var _innerPadding = 30;
		
		self.htmlElement = container.append('div')
			.attr('id', _name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');		
		
		var _min = options.min ? options.min : 0;
		self.min = function(val) {	
			if (val !== undefined) {
				_min = val;
				self.redraw();
			} else {
				return _min;
			}	
		}

		var _max = options.max ? options.max : 100;
		self.max = function(val) {		
			if (val !== undefined) {
				_max = val;
				self.redraw();
			} else {
				return _max;
			}
		}
		
		var _color = options.color ? options.color : '#ff0000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}
		
		var _r = options.r ? options.r : 75;
		self.r = function(val) {
			if (val !== undefined) {
				_r = val;
				self.redraw();
			} else {
				return _r;
			}
		}	
				
		self.width = function() { return 2*self.r(); };
		self.height = function() { return 2*self.r(); };

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
			
		//console.log(eval("function comp(val) { return (val*3600/100).toFixed(0) + 'kmph'; }"))	
		var _math = options.math ? Function("val", 'return ' + options.math + ';') : undefined;
		self.math = function(val) {
			if (val !== undefined) {
				_math = eval(val);
				self.redraw();
			} else {
				return _math;
			}			
		} 

		var _percentage = options.percentage ? options.percentage : false; 
		self.percentage = function(val) { 
			if (val !== undefined) {
				_percentage = val;
				self.redraw();
			} else {
				return _percentage;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				if (!gauge || !svgtxt) return;
				if (self.min() < 0) {
					if (val < 0) {
						data = [ self.max()/range, 
								 -val/range, 
								 (-self.min()+val)/range ];
					} else {
						data = [ (self.max()-val)/range, 
								 val/range, 
								 -self.min()/range ];
					}
				} else {
					data = [ self.min()/range, 
							 (self.max()-val-self.min())/range, 
							 (val-self.min())/range ];
				}
				gauge = gauge.data(donut(data)); // update the data
				gauge.attr('d', arcgen); // redraw the arcs
				if (self.percentage()) {
					svgtxt.text((val/range*100).toFixed(self.accuracy()) + '%');
				} else {
					svgtxt.text(val.toFixed(self.accuracy()));
				}
				if (self.math()) {
					svgtxt_sec.text(self.math()(val));
				}
			} else {
				return _value;
			}
		}
		
		var range, data, donut, colors, arcgen;
		var gauge, svgroot, svggroup, svgtxt, svgtxt_sec, svgNametxt;
		var gA = 2*π - 1/3 * π;
		var dA = 3*π/4;

		self.draw = function() {
			range = self.max() - self.min();
			donut = d3.layout.pie().sort(null);
			donut.startAngle(3*π/4).endAngle(-3*π/4);
			
			if (self.min() < 0) {
				colors = ['#aaaaaa', self.color(), '#aaaaaa'];
				data = [self.max()/range, 
						 self.value()/range,
						 (-self.min()-self.value())/range ];
			} else {
				colors = ['#aaaaaa', '#aaaaaa', self.color()];
				data = [(self.max()-self.value()+self.min())/range, 
						(self.value()-self.min())/range,  
						self.min()/range];
			}

			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', 2*self.r()+2*_innerPadding)
				.attr('height', 2*self.r()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');

			svgroot.append('svg:title').text('Name: ' + self.name());
			
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.r()+_innerPadding)+
					','+(self.r()+_innerPadding)+')');
			
			arcgen = d3.svg.arc().innerRadius(3*self.r()/4).outerRadius(self.r());
			
			gauge = svggroup.selectAll('path')
				.data(donut(data))
			  .enter().append('svg:path')
				.attr('x', self.x())
				.attr('y', self.y())
				.attr('fill', function(d, i) { return colors[i]; })
				.attr('d', arcgen)
				.each(function(d) { this._current = d; });
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', self.r()/3)
				.style('text-anchor', 'middle')
				.attr('dy', '0.25em')
				.text(0);
			
			if (self.math()) {
				svgtxt_sec = svggroup.append('svg:text')
					.style('font-family', 'DigitaldreamRegular')
					.style('font-size', self.r()/8)
					.style('text-anchor', 'middle')
					.attr('y', self.r()/3)
					.attr('dy', '0.25em')
					.text(0);
			}

			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'middle')
				.attr('y', self.r() + 20)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', 3*self.r()/4)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', self.r())
				.style('fill-opacity', 0)
				.style('stroke', '#333333');
			
			// min/max tick
			svggroup.append('svg:line')
				.attr('x1', 3*self.r()/4*cos(-dA-π/2))
				.attr('y1', 3*self.r()/4*sin(-dA-π/2))
				.attr('x2', (self.r()+20)*cos(-dA-π/2))
				.attr('y2', (self.r()+20)*sin(-dA-π/2))
				.style('stroke', '#333333');
				
			svggroup.append('svg:line')
				.attr('x1', 3*self.r()/4*cos(dA-π/2))
				.attr('y1', 3*self.r()/4*sin(dA-π/2))
				.attr('x2', (self.r()+20)*cos(dA-π/2))
				.attr('y2', (self.r()+20)*sin(dA-π/2))
				.style('stroke', '#333333');	

			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'start')
				.attr('x', (self.r()+20)*cos(dA-π/2)-2)
				.attr('y', (self.r()+20)*sin(dA-π/2)-12)
				.text(self.max().toString());
			
			// min text
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '0.9em')
				.style('text-anchor', 'end')
				.attr('x', (self.r()+20)*cos(-dA-π/2)-2)
				.attr('y', (self.r()+20)*sin(-dA-π/2)-2)
				.text(self.min().toString());
			
			if (self.min() < 0) {
				svggroup.append('svg:line')
					.attr('x1', 3/4*self.r()*cos(-dA-π+3*π/2*self.min()/range))
					.attr('y1', 3/4*self.r()*sin(-dA-π+3*π/2*self.min()/range))
					.attr('x2', (self.r()+10)*cos(-dA-π+3*π/2*self.min()/range))
					.attr('y2', (self.r()+10)*sin(-dA-π+3*π/2*self.min()/range))
					.style('stroke', '#333333');
				
				var svg0txt = svggroup.append('svg:text')
					.attr('x', self.r()*cos(-dA-π+3*π/2*self.min()/range))
					.attr('y', (self.r()+12)*sin(-dA-π+3*π/2*self.min()/range))
					.style('text-anchor', 'start') 
					.style('font-family', 'sans-serif')
					.style('font-size', '1em')
					.text(0);
			}		
		}
		
		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}
	
		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();
		self.value(_value);
		self.x(_x);
		self.y(_y);
	}
	// end of PieGaugeCASCO






	// BoxGauge
	uruburu.BoxGauge = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'min', 'max', 'color', 'x', 'y', 
			'width', 'height', 'accuracy', 'percentage', 'orientation'];

		self.type = 'BoxGauge';
		
		var _name = options.name || 'box_gauge_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = (_orientation == 'vertical' ? 
			(options.height ? options.height*2 : 40) : (options.width ? options.width*2 : 40) );
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
		
		var _orientation = options.orientation ? options.orientation : 'vertical';
		self.orientation = function(val) {
			if (val !== undefined) {
				var tmpw = _width;
				_width = _height;
				_height = tmpw;
				self.redraw();
			} else {
				return _orientation;
			}
		}
	
		var _width = options.width ? options.width : (_orientation == 'vertical' ? 20 : 160);
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : (_orientation == 'vertical' ? 160 : 20);
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
	
		var _min = options.min ? options.min : 0;
		self.min = function(val) {	
			if (val !== undefined) {
				_min = val;
				self.redraw();
			} else {
				return _min;
			}	
		}

		var _max = options.max ? options.max : 100;
		self.max = function(val) {		
			if (val !== undefined) {
				_max = val;
				self.redraw();
			} else {
				return _max;
			}
		}
		
		var _color = options.color ? options.color : '#ff0000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
			
		var _percentage = options.percentage ? options.percentage : false; 
		self.percentage = function(val) { 
			if (val !== undefined) {
				_percentage = val;
				self.redraw();
			} else {
				return _percentage;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (!svgtxt || !gauge) return;
			if (self.orientation() == 'vertical') {
				if (val >= 0) {
					gauge.attr('y', (self.height()+self.min()*self.height()/range)-val*self.height()/range)
						.attr('height', self.height()-(self.max()-val-self.min())*self.height()/range);
				} else {
					gauge.attr('y', (self.height()+self.min()*self.height()/range))
						.attr('height', -val*self.height()/range);
				}
			} else {
				if (val >= 0) {
					gauge.attr('x', -self.min()*self.width()/range)
						.attr('width', val*self.width()/range);
				} else {
					gauge.attr('x', (-self.min()+val)*self.width()/range)
						.attr('width', -val*self.width()/range);
				}
			}
	
			var str;
			if (self.percentage()) {
				str = (val/range*100).toFixed(0) + '%';
			} else {
				str = val.toFixed(self.accuracy());
			}
			svgtxt.style('font-size', Math.min(frameRadius/20*12, frameRadius/20*12*3/str.length)).text(str);
		}
		
		var range, frameRadius, fontSize, svgroot, gauge, svgtxt, svggroup, svgNametxt;
	
		self.draw = function() {
			range = self.max() - self.min();
	
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');

			svgroot.append('svg:title').text('Name: ' + self.name());
					
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(_innerPadding)+
					','+(_innerPadding)+')');
			
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.style('fill', '#aaaaaa')
				.style('stroke-opacity', 0);
			
			gauge = svggroup.append('svg:rect');
			
			// final frame
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.style('fill-opacity', 0)
				.style('stroke', '#333333');
			
			
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'start')
				.attr('x', 0)
				.attr('y', self.height() + 16)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			if (self.orientation() == 'vertical') {
				gauge.attr('fill', self.color())
					.attr('y', self.height())
					.attr('width', self.width())
					.attr('height', 0);
				frameRadius = self.width();
				fontSize = self.width()/2;
				
				// min tick
				svggroup.append('svg:line')
					.attr('x1', self.width())
					.attr('y1', self.height())
					.attr('x2', self.width()+10)
					.attr('y2', self.height())
					.style('stroke', '#333333');
			
				svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'start')
				.attr('x', self.width()+12)
				.attr('y', self.height())
				.attr('dy', '0.25em')
				.text(self.min().toString());
				
				// max tick
				svggroup.append('svg:line')
					.attr('x1', -10)
					.attr('y1', 0)
					.attr('x2', 0)
					.attr('y2', 0)
					.style('stroke', '#333333');
				
				svggroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', '1em')
					.style('text-anchor', 'end')
					.attr('x', -12)
					.attr('y', 0)
					.attr('dy', '0.25em')
					.text(self.max().toString());
				
				if (self.min() < 0) {
					svggroup.append('svg:line')
					.attr('x1', -10)
					.attr('y1', self.height()+self.min()*self.height()/range)
					.attr('x2', self.width())
					.attr('y2', self.height()+self.min()*self.height()/range)
					.style('stroke', '#333333');
					
					svggroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', '1em')
					.style('text-anchor', 'end')
					.attr('x', -12)
					.attr('y', self.height()+self.min()*self.height()/range)
					.attr('dy', '0.25em')
					.text(0);
				}
			} else {
				gauge.attr('fill', self.color())
					.attr('width', 0)
					.attr('height', self.height());
				frameRadius = self.height();
				fontSize = self.height()/2;
				
				// min tick
				svggroup.append('svg:line')
					.attr('x1', 0)
					.attr('y1', 0)
					.attr('x2', 0)
					.attr('y2', -10)
					.style('stroke', '#333333');
		
				svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'middle')
				.attr('x', 0)
				.attr('y', -12)
				.text(self.min().toString());
				
				// max tick
				svggroup.append('svg:line')
					.attr('x1', self.width())
					.attr('y1', self.height())
					.attr('x2', self.width())
					.attr('y2', self.height()+10)
					.style('stroke', '#333333');
				
				svggroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', '1em')
					.style('text-anchor', 'middle')
					.attr('x', self.width())
					.attr('y', self.height()+22)
					.text(self.max().toString());
				
				if (self.min() < 0) {
					svggroup.append('svg:line')
					.attr('x1', -self.min()*self.width()/range)
					.attr('y1', 0)
					.attr('x2', -self.min()*self.width()/range)
					.attr('y2', self.height()+10)
					.style('stroke', '#333333');
					
					svggroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', '1em')
					.style('text-anchor', 'middle')
					.attr('x', -self.min()*self.width()/range)
					.attr('y', self.height()+22)
					.text(0);
				}
			}
			
			svggroup.append('svg:circle')
				.attr('cx', self.width()+frameRadius/3)
				.attr('cy', -frameRadius/3)
				.attr('r', frameRadius)
				.style('fill', 'white')
				.style('stroke-opacity', 0);
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', '1em')
				.style('text-anchor', 'middle')
				.attr('x', self.width()+frameRadius/3)
				.attr('y', -frameRadius/3)
				.attr('dy', '0.25em');

			svggroup.append('svg:circle')
				.attr('cx', self.width()+frameRadius/3)
				.attr('cy', -frameRadius/3)
				.attr('r', frameRadius)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');		
		}

		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}

		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();		
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
		self.value(_value);
	}
	// end of BoxGauge










	// ToggleButton
	uruburu.ToggleButton = function(container,options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'x', 'y', 'width', 'height'];
		self.type = 'ToggleButton';
		
		var _name = options.name || 'toggle_button_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = 45;
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
	
		var _width = options.width ? options.width : 60;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 30;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {	
			if (val !== undefined) {
				_value = val;
				button.transition().duration(1000)
					.attr('transform', 'translate(' + (_value ? self.width()/2 : 0) + ',0)');
				svgtxt.text(_value ? 'ON': 'OFF');
				uruburu.emit('change', self.bind, _value);
			} else {
				return _value;
			}
		}

		var svgroot, svggroup, svgtxt, svgNametxt, button;
		
		self.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);
				
			var defs = svgroot.append('svg:defs'),
				macblueLG = defs.append('linearGradient').attr('id', 'macblue');
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#6DCAF2');
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#1a82f7');

			svgroot.append('svg:title').text('Name: ' + self.name());
					
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(_innerPadding)+
					','+(_innerPadding)+')');
	
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', self.height()/2)
				.style('fill', 'white')
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:circle')
				.attr('cx', self.width() + 8)
				.attr('cy', -self.height()/2 + 1)
				.attr('r', 20)
				.style('fill', 'white')
				.style('stroke-opacity', 0);
		
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', self.width() + 8)
				.attr('y', -self.height()/2 + 1)
				.attr('dy', '0.25em')
				.text('OFF');
			
			svggroup.append('svg:circle')
				.attr('cx', self.width() + 8)
				.attr('cy', -self.height()/2 + 1)
				.attr('r', 20)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333');	
		
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', self.width()/2)
				.attr('y', self.height() + 8)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			button = svggroup.append('svg:rect')
				.attr('width', self.height())
				.attr('height', self.height())
				.attr('rx', self.height()/2)
				.style('fill', 'url(#macblue)')
				.style('stroke', '#333333')
				.style('cursor', 'pointer')
				.on('click', function() {
					self.value(!self.value());
				});
		}

		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}

		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();		
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
		self.value(_value);
	}
	// end of ToggleButton








	// Led
	uruburu.Led = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'x', 'y', 'color'];
		self.type = 'Led';
		
		var _name = options.name || 'led_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = 18;
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
	
		var _width = options.width ? options.width : 30;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 30;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
		
		var colors = ['red', 'yellow', 'green', 'orange'];
		var _color = options.color ? options.color : colors[0];
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}
		
		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {	
			if (val !== undefined) {
				_value = val;
				if (val) {
					ledLight.style('fill', 'url(#led' + self.color() +')')
						.style('fill-opacity', 0.8);
				} else { 
					ledLight.style('fill', 'url(#blueglass)')
						.style('fill-opacity', 0.3);	
				}
			} else {
				return _value;
			}
		}

		var svgroot, svggroup, ledLight, svgNametxt, gradient, defs;

		this.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);
				
			svgroot.append('svg:title').text('Name: ' + self.name());
			
			defs = svgroot.append('svg:defs');
			var macblueLG = defs.append('linearGradient').attr('id', 'macblue');
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#6DCAF2');
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#1a82f7');

			gradient = defs.append('radialGradient').attr('id', 'led'+self.color());
			gradient.append('svg:stop').attr('offset', '5%').style('stop-color', '#ffffff');
			gradient.append('svg:stop').attr('offset', '90%').style('stop-color', self.color());
			
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.width()/2+_innerPadding)+
					','+(self.height()/2+_innerPadding)+')');
			
			svggroup.append('svg:circle')
				.attr('r', 20)
				.style('fill', 'white')
				.style('stroke-opacity', 0);
		
			ledLight = svggroup.append('svg:circle')
				.attr('r', 20)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333');	
	
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', 0)
				.attr('y', 28)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name())	
		}

		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}

		self.remove = function() {
			self.htmlElement.remove();
		}
				
		self.draw();		
		self.color(_color);
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
		self.value(_value);
	}
	// end of Led










	// BoxLever
	uruburu.BoxLever = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'min', 'max', 'color', 'x', 'y', 
			'width', 'height', 'accuracy', 'percentage', 'orientation'];

		self.type = 'BoxLever';
		
		var _name = options.name || 'box_lever_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = (_orientation == 'vertical' ? 
			(options.height ? options.height*2 + 10  : 50) : (options.width ? options.width*2 + 10 : 50) );
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
		
		var _orientation = options.orientation ? options.orientation : 'vertical';
		self.orientation = function(val) {
			if (val !== undefined) {
				_width = options.width ? options.width : (val == 'vertical' ? 20 : 160);
				_height = options.height ? options.height : (val=='vertical' ? 160 : 20);
				self.redraw();
			} else {
				return _orientation;
			}
		}
	
		var _width = options.width ? options.width : (_orientation == 'vertical' ? 20 : 160);
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : (_orientation == 'vertical' ? 160 : 20);
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
	
		var _min = options.min ? options.min : 0;
		self.min = function(val) {	
			if (val !== undefined) {
				_min = val;
				self.redraw();
			} else {
				return _min;
			}	
		}

		var _max = options.max ? options.max : 100;
		self.max = function(val) {		
			if (val !== undefined) {
				_max = val;
				self.redraw();
			} else {
				return _max;
			}
		}
		
		var _color = options.color ? options.color : '#ff0000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
			
		var _percentage = options.percentage ? options.percentage : false; 
		self.percentage = function(val) { 
			if (val !== undefined) {
				_percentage = val;
				self.redraw();
			} else {
				return _percentage;
			}
		}	
				
		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				if (!gauge || !svgtxt || isNaN(val)) return;

				if (self.orientation() == 'vertical') {
					gauge.attr('y2', self.height()+(self.min()-val)*self.height()/range);
				} else {
					gauge.attr('x2', (val-self.min())*self.width()/range);
				}
				
				var str;
				if (self.percentage()) {
					str = (val/range*100).toFixed(0) + '%';
				} else {
					str = val.toFixed(self.accuracy());
				}
				svgtxt.style('font-size', Math.min(12, 12*3/str.length)).text(str);		
				uruburu.emit('change', self.bind, _value);
			} else {
				return _value;
			}
		}

		var range, frameRadius, fontSize, 
			svgroot, gauge, handler, svgtxt, svggroup, gradgroup, svgNametxt;
	
		self.draw = function() {
			range = self.max() - self.min();
	
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				macblueLG = defs.append('linearGradient').attr('id', 'macblue');
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#6DCAF2');
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#1a82f7');

			svgroot.append('svg:title').text('Name: ' + self.name());
					
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(_innerPadding)+
					','+(_innerPadding)+')');
			
			gradgroup = svggroup.append('svg:g');

			handler = svggroup.append('svg:g')
				.style('cursor', 'pointer')
				.style('pointer-events', 'auto');

			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'start')
				.attr('x', 0)
				.attr('y', self.height() + 16)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			var initOffX, initOffY, posX, posY;
			
			if (self.orientation() == 'vertical') {
				var handlerDrag = d3.behavior.drag()
					.on('dragstart', function(d, i) {
						initOffY = d3.event.sourceEvent.pageY;
						if (handler.attr('transform')) {
							posY = parseInt(handler
									.attr('transform')
									.replace('translate(','')
									.replace(')','')
									.split(',')[1]);
						} else {
							posY = 0;
						}
					})
					.on('drag', function(d, i) {
						var newPosY = posY + (d3.event.sourceEvent.pageY - initOffY);

						if (newPosY >= -self.max()/range*self.height() && 
								newPosY <= -self.min()/range*self.height()) {
							handler.attr('transform', 'translate(0,'  + newPosY + ')');
							self.value(-newPosY*range/self.height());
						}  else {
							if (newPosY < -self.max()/range*self.height()) {
								self.value(self.max());
								handler.attr('transform', 'translate(0,'  + (-self.max()/range*self.height()) + ')');
							}
							if (newPosY > -self.min()/range*self.height()) {
								self.value(self.min());
								handler.attr('transform', 'translate(0,'  + (-self.min()/range*self.height()) + ')');
							}
						}
					});
				
				handler.call(handlerDrag);
				
				// track
				svggroup.append('svg:line')
					.attr('x1', self.width()/2)
					.attr('y1', 0)
					.attr('x2', self.width()/2)
					.attr('y2', self.height())
					.style('stroke', '#333333')
					.style('stroke-width', 2);
			
				gauge = svggroup.append('svg:line')
					.attr('stroke', self.color())
					.attr('stroke-width', 3)
					.attr('x1', self.width()/2)
					.attr('y1', self.height()+self.min()*self.height()/range)
					.attr('x2', self.width()/2)
					.attr('y2', self.height()+self.min()*self.height()/range);
				
				frameRadius = self.width();
				fontSize = self.width()/2;
				
				// min tick
				gradgroup.append('svg:line')
					.attr('x1', self.width()/2-10)
					.attr('y1', self.height())
					.attr('x2', self.width()+10)
					.attr('y2', self.height())
					.style('stroke', '#333333');
			
				gradgroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', 12)
					.style('text-anchor', 'start')
					.attr('x', self.width()+12)
					.attr('y', self.height())
					.attr('dy', '0.25em')
					.text(self.min().toString());
				
				// max tick
				gradgroup.append('svg:line')
					.attr('x1', -10)
					.attr('y1', 0)
					.attr('x2', self.width()/2+10)
					.attr('y2', 0)
					.style('stroke', '#333333');
				
				gradgroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', 12)
					.style('text-anchor', 'end')
					.attr('x', -12)
					.attr('y', 0)
					.attr('dy', '0.25em')
					.text(self.max().toString());
				
				if (self.min() < 0) {
					svggroup.append('svg:line')
						.attr('x1', -10)
						.attr('y1', self.height()+self.min()*self.height()/range)
						.attr('x2', self.width())
						.attr('y2', self.height()+self.min()*self.height()/range)
						.style('stroke', '#333333');
					
					svggroup.append('svg:text')
						.style('font-family', 'sans-serif')
						.style('font-size', 12)
						.style('text-anchor', 'end')
						.attr('x', -12)
						.attr('y', self.height()+self.min()*self.height()/range)
						.attr('dy', '0.25em')
						.text(0);
				}
				
				handler.append('svg:line')
					.attr('x1', -5)
					.attr('y1', self.height()+self.min()*self.height()/range)
					.attr('x2', self.width())
					.attr('y2', self.height()+self.min()*self.height()/range)
					.style('stroke', '#333333')
					.style('stroke-width', 2);		
				
				handler.append('svg:rect')
					.attr('x', self.width()/2+10)
					.attr('y', 
						self.height()+self.min()*self.height()/range-self.width()/3)
					.attr('width', 2*self.width())
					.attr('height', self.width()/1.5)
					.attr('rx', self.width()/4)
					.style('fill', 'url(#macblue)')
					.style('stroke', '#333333');
				
			} else {
				var handlerDrag = d3.behavior.drag()
					.on('dragstart', function(d, i) {
						initOffX = d3.event.sourceEvent.pageX;
						if (handler.attr('transform')) {
							posX = parseInt(handler
									.attr('transform')
									.replace('translate(','')
									.replace(')','')
									.split(',')[0]);
						} else {
							posX = 0;
						}
					})
					.on('drag', function(d, i) {
						var newPosX = posX + (d3.event.sourceEvent.pageX - initOffX);
						if (newPosX >= self.min()/range*self.width() && 
								newPosX <= self.max()/range*self.width()) {
							handler.attr('transform', 'translate('  + newPosX + ',0)');
							self.value(newPosX*range/self.width(), true);
						} else {
							if (newPosX < self.min()/range*self.width()) {
								self.value(self.min());
								handler.attr('transform', 'translate('  + self.min()/range*self.width() + ',0)');
							}
							if (newPosX > self.max()/range*self.width()) {
								self.value(self.max());
								handler.attr('transform', 'translate('  + self.max()/range*self.width() + ',0)');
							}
						}
					});
				
				handler.call(handlerDrag);	
				
				// track
				svggroup.append('svg:line')
					.attr('x1', 0)
					.attr('y1', self.height()/2)
					.attr('x2', self.width())
					.attr('y2', self.height()/2)
					.style('stroke', '#333333')
					.style('stroke-width', 2);
				
				gauge = svggroup.append('svg:line')
					.attr('stroke', self.color())
					.attr('stroke-width', 3)
					.attr('x1', -self.min()*self.width()/range)
					.attr('y1', self.height()/2)
					.attr('x2', -self.min()*self.width()/range)
					.attr('y2', self.height()/2);
				
				frameRadius = self.height();
				fontSize = self.height()/2;
				
				// min tick
				gradgroup.append('svg:line')
					.attr('x1', 0)
					.attr('y1', -10)
					.attr('x2', 0)
					.attr('y2', self.height()/2+10)
					.style('stroke', '#333333');
		
				gradgroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', 12)
					.style('text-anchor', 'middle')
					.attr('x', 0)
					.attr('y', -12)
					.text(self.min().toString());
				
				// max tick
				gradgroup.append('svg:line')
					.attr('x1', self.width())
					.attr('y1', self.height()/2-10)
					.attr('x2', self.width())
					.attr('y2', self.height()+10)
					.style('stroke', '#333333');
				
				gradgroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', 12)
					.style('text-anchor', 'middle')
					.attr('x', self.width())
					.attr('y', self.height()+22)
					.text(self.max().toString());
				
				if (self.min() < 0) {
					svggroup.append('svg:line')
						.attr('x1', -self.min()*self.width()/range)
						.attr('y1', 0)
						.attr('x2', -self.min()*self.width()/range)
						.attr('y2', self.height()+10)
						.style('stroke', '#333333');
					
					svggroup.append('svg:text')
						.style('font-family', 'sans-serif')
						.style('font-size', 12)
						.style('text-anchor', 'middle')
						.attr('x', -self.min()*self.width()/range)
						.attr('y', self.height()+22)
						.text(0);
				}
				
				handler.append('svg:line')
					.attr('x1', -self.min()*self.width()/range)
					.attr('y1', -self.height()/2)
					.attr('x2', -self.min()*self.width()/range)
					.attr('y2', self.height()/2+10)
					.style('stroke', '#333333')
					.style('stroke-width', 2);		
				
				handler.append('svg:rect')
					.attr('x', -self.min()*self.width()/range-self.height()/3)
					.attr('y', -4*self.height()/3-13)
					.attr('width', self.height()/1.5)
					.attr('height', 2*self.height())
					.attr('rx', self.height()/4)
					.style('fill', 'url(#macblue)')
					.style('stroke', '#333333');
			}
		
			var cxpos, cypos;
			if (self.orientation() == 'vertical') {
				cxpos = frameRadius/2;
				cypos = -frameRadius;
			} else {
				cxpos = self.width() + frameRadius;
				cypos = self.height()/2;	
			} 
			
			svggroup.append('svg:circle')
				.attr('cx', cxpos)
				.attr('cy', cypos)
				.attr('r', frameRadius)
				.style('fill', 'white')
				.style('stroke-opacity', 0);
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', cxpos)
				.attr('y', cypos)
				.attr('dy', '0.25em')
				.text(0);
			
			svggroup.append('svg:circle')
				.attr('cx', cxpos)
				.attr('cy', cypos)
				.attr('r', frameRadius)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333');		
		};

		self.redraw = function() {
			svgroot.remove();
			self.draw();
			self.value(self.value());
		}
		
		self.remove = function() {
			self.htmlElement.remove();
		}
	
		self.draw();		
		self.value(_value);
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
	}	
	// end of BoxLever








	
	// Stepper
	uruburu.Stepper = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'x', 'y', 'width', 'height', 'accuracy', 'step'];

		self.type = 'Stepper';
		
		var _name = options.name || 'stepper_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = 45;
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
	
		var _width = options.width ? options.width : 90;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 60;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _step = options.step ? options.step : 1;
		self.step = function(val) {
			if (val !== undefined) {
				_step = val;
			} else {
				return _step;
			}			
		} 

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
				
		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				var str = self.value().toFixed(self.accuracy());
				svgtxt.style('font-size', Math.min(12, 12*4/str.length)).text(str);
				uruburu.emit('change', self.bind, _value);	
			} else {
				return _value;
			}
		}

		var svgroot, svggroup, svgtxt, svgNametxt;
	
		self.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);

			var defs = svgroot.append('svg:defs');
			var blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');
			
			var macgreenLG = defs.append('linearGradient').attr('id', 'macgreen');
			macgreenLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#5D9426');
			macgreenLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#6CD900');
			
			var macredLG = defs.append('linearGradient').attr('id', 'macred');
			macredLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#F00A0A');
			macredLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#FF5252');	

			svgroot.append('svg:title').text('Name: ' + self.name());
					
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.width()/2+_innerPadding)+
					','+(self.height()/2+_innerPadding)+')');
			
			svggroup.append('svg:circle')
				.attr('r', 25)
				.style('fill', 'white')
				.style('stroke-opacity', 0);
		
			svggroup.append('svg:circle')
				.attr('r', 25)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333');	
	
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', 0)
				.attr('y', 0)
				.attr('dy', '0.25em')
				.text(0);
			
			// plus button
			svggroup.append('svg:circle')
				.attr('cx', 25)
				.attr('cy', 25)
				.attr('r', 10)
				.style('fill', 'url(#macgreen)')
				.style('stroke', '#333333')
				.style('cursor', 'pointer')
				.on('click', function() {
					d3.event.stopPropagation();
					d3.event.preventDefault();
					self.value(self.value()+self.step());
				});	
			
			// minus button
			svggroup.append('svg:circle')
				.attr('cx', -25)
				.attr('cy', 25)
				.attr('r', 10)
				.style('fill', 'url(#macred)')
				.style('stroke', '#333333')
				.style('cursor', 'pointer')
				.on('click', function() {			
					d3.event.stopPropagation();
					d3.event.preventDefault();
					self.value(self.value()-self.step());
				});	
			
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', 0)
				.attr('y', 48)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.style('pointer-events', 'none')
				.text(self.name());
			
		}


		self.redraw = function() {
			svgroot.remove();
			self.draw();
			self.value(self.value());
		}

		self.remove = function() {
			self.htmlElement.remove();
		}

		self.draw();		
		self.value(_value);
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
	}	
	// end of Stepper
	






	
	//							  	           AngularGauge
	// -----------------------------------------------------------------------------------	
	uruburu.AngularGauge = function (container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'color', 'x', 'y', 'r', 'accuracy'];
		self.type = 'AngularGauge';
		
		var _name = options.name || 'angular_gauge_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				self.redraw();
			} else {
				return _name;
			}
		}

		var _innerPadding = 30;
		
		self.htmlElement = container.append('div')
			.attr('id', _name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');		
		
		var _color = options.color ? options.color : '#ff0000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}
		
		var _r = options.r ? options.r : 75;
		self.r = function(val) {
			if (val !== undefined) {
				_r = val;
				self.redraw();
			} else {
				return _r;
			}
		}	
				
		self.width = function() { return 2*self.r(); };
		self.height = function() { return 2*self.r(); };

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				if (gauge && svgtxt)  {
					gauge.transition().delay(100)
						.attr('transform', 'rotate(' + -val + ')');
					var str = val.toFixed(self.accuracy())+'°';
					svgtxt.style('font-size', Math.min(32, 32*3/str.length)).text(str);
				}
				uruburu.emit('change', self.bind, _value);
			} else {
				return _value;
			}
		}
		
		var gauge, svgroot, svggroup, svgtxt, svgNametxt;
		
		self.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', 2*self.r()+2*_innerPadding)
				.attr('height', 2*self.r()+2*_innerPadding);
				
			var defs = svgroot.append('svg:defs'),
				blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');

			svgroot.append('svg:title').text('Name: ' + self.name());
			
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.r()+_innerPadding)+
					','+(self.r()+_innerPadding)+')');
			
			gauge = svggroup.append('path')
				.attr('x', self.x())
				.attr('y', self.y())
				.attr('fill', self.color())
				.attr('d', 
					'M -'+self.r()+',0 A'+self.r()+','+self.r()+' 0 0 0 '+self.r()+',0')
				.each(function(d) { this._current = d; });
			
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('y', self.r()+20)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			// horizont line
			svggroup.append('svg:line')
				.attr('x1', -self.r()-10)
				.attr('y1', 0)
				.attr('x2', self.r()+10)
				.attr('y2', 0)
				.style('stroke', '#333333');
		
			// frame and co.
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', 2.5*self.r()/4)
				.style('fill', 'white');
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', self.r()/3)
				.style('text-anchor', 'middle')
				.attr('dx', '0.25em')
				.attr('dy', '0.25em')
				.text('0°');
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', 2.5*self.r()/4)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', self.r())
				.style('fill-opacity', 0)
				.style('stroke', '#333333');
		}
		
		self.redraw = function() {
			svgroot.remove();
			iself.draw();
			self.value(self.value());
		}
			
		self.remove = function() {
			this.htmlElement.remove();
		}
		
		self.draw();
		self.value(_value);
		self.x(_x);
		self.y(_y);
	}
	// end of AngularGauge










	// PieDial
	// -----------------------------------------------------------------------------------
	uruburu.PieDial = function (container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'min', 'max', 'color', 'x', 'y', 'r', 'accuracy', 'percentage'];
		self.type = 'PieDial';
		
		var _name = options.name || 'pie_dial_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				self.redraw();
			} else {
				return _name;
			}
		}

		var _innerPadding = 25;
		
		self.htmlElement = container.append('div')
			.attr('id', _name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');		
		
		var _min = options.min ? options.min : 0;
		self.min = function(val) {	
			if (val !== undefined) {
				_min = val;
				self.redraw();
			} else {
				return _min;
			}	
		}

		var _max = options.max ? options.max : 100;
		self.max = function(val) {		
			if (val !== undefined) {
				_max = val;
				self.redraw();
			} else {
				return _max;
			}
		}
		
		var _color = options.color ? options.color : '#ff0000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}
		
		var _r = options.r ? options.r : 75;
		self.r = function(val) {
			if (val !== undefined) {
				_r = val;
				self.redraw();
			} else {
				return _r;
			}
		}	
				
		self.width = function() { return 2*self.r(); };
		self.height = function() { return 2*self.r(); };

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
			
		var _percentage = options.percentage ? options.percentage : false; 
		self.percentage = function(val) { 
			if (val !== undefined) {
				_percentage = val;
				self.redraw();
			} else {
				return _percentage;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				if (!gauge || !svgtxt) return;
				if (self.min() < 0) {
					if (val < 0) {
						data = [ self.max()/range, 
								 -val/range, 
								 (-self.min()+val)/range ];
					} else {
						data = [ (self.max()-val)/range, 
								 val/range, 
								 -self.min()/range ];
					}
				} else {
					data = [ self.min()/range, 
							 (self.max()-val-self.min())/range, 
							 (val-self.min())/range ];
				}
				gauge = gauge.data(donut(data)); // update the data
				gauge.attr('d', arcgen); // redraw the arcs
				if (self.percentage()) {
					svgtxt.text((val/range*100).toFixed(self.accuracy()) + '%');
				} else {
					svgtxt.text(val.toFixed(self.accuracy()));
				}
				uruburu.emit('change', self.bind, _value);
			} else {
				return _value;
			}
		}
		
		var range, data, donut, colors;
		var arcgen, gauge, svgroot, svggroup, svgtxt, svgNametxt;
		
		self.draw = function() {
			range = self.max() - self.min();
			donut = d3.layout.pie().sort(null);
			
			if (self.min() < 0) {
				colors = ['#aaaaaa', self.color(), '#aaaaaa'];
				data = [ self.max()/range, 
						 self.value()/range,
						 (-self.min()-self.value())/range ];
			} else {
				colors = ['#aaaaaa', '#aaaaaa', self.color()];
				data = [(self.max()-self.value()+self.min())/range, 
						(self.value()-self.min())/range,  
						self.min()/range];
			}
			
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', 2*self.r()+2*_innerPadding)
				.attr('height', 2*self.r()+2*_innerPadding);

			var defs = svgroot.append('svg:defs');
			var blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');
			var macblueLG = defs.append('linearGradient').attr('id', 'macblue');
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#6DCAF2');
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#1a82f7');

			svgroot.append('svg:title').text('Name: ' + self.name());
			
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.r()+_innerPadding)+
					','+(self.r()+_innerPadding)+')');
			
			arcgen = d3.svg.arc().innerRadius(3*self.r()/4).outerRadius(self.r());
			
			gauge = svggroup.selectAll('path')
				.data(donut(data))
			  .enter().append('svg:path')
				.attr('x', self.x())
				.attr('y', self.y())
				.attr('fill', function(d, i) { return colors[i]; })
				.attr('d', arcgen)
				.each(function(d) { this._current = d; });
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', self.r()/3)
				.style('text-anchor', 'middle')
				.attr('dy', '0.25em')
				.text(0);
			
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('y', self.r() + 20)
				.attr('fill', '#999999')
				.attr('dy', '0.25em')
				.text(self.name());
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', 3*self.r()/4)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', self.r())
				.style('fill-opacity', 0)
				.style('stroke', '#333333');
			
			// min/max tick
			svggroup.append('svg:line')
				.attr('x1', 0)
				.attr('y1', -3*self.r()/4)
				.attr('x2', 0)
				.attr('y2', -self.r()-20)
				.style('stroke', '#333333');
		
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'start')
				.attr('x', 2)
				.attr('y', -self.r()-12)
				.text(self.max().toString());
			
			// min text
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'end')
				.attr('x', -2)
				.attr('y', -self.r()-2)
				.text(self.min().toString());
			
			if (self.min() < 0) {
				svggroup.append('svg:line')
					.attr('x1', 3/4*self.r()*cos(2*π+2*π*self.min()/range-π/2))
					.attr('y1', 3/4*self.r()*sin(2*π+2*π*self.min()/range-π/2))
					.attr('x2', (self.r()+10)*cos(2*π+2*π*self.min()/range-π/2))
					.attr('y2', (self.r()+10)*sin(2*π+2*π*self.min()/range-π/2))
					.style('stroke', '#333333');
				
				var svg0txt = svggroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', 12)
					.text(0);
				
				if (Math.abs(2*π+2*π*self.min()/range-π/2) > π/2) {
					svg0txt.style('text-anchor', 'end')
						.attr('x', (self.r()+12)*cos(2*π+2*π*self.min()/range-π/2) - 2)
						.attr('y', (self.r()+12)*sin(2*π+2*π*self.min()/range-π/2));
				} else if (Math.abs(2*π+2*π*self.min()/range-π/2) < π/2) {
					svg0txt.style('text-anchor', 'start')
						.attr('x', (self.r()+12)*cos(2*π+2*π*self.min()/range-π/2) + 2)
						.attr('y', (self.r()+12)*sin(2*π+2*π*self.min()/range-π/2));
				} else {
					svg0txt.style('text-anchor', 'middle');
				}
			}	
				
			var initOffX, initOffY, initA;
			var handlerDrag = d3.behavior.drag()
				.on('dragstart', function(d, i) {
					initOffX = d3.event.sourceEvent.pageX - self.x() - self.r();
					initOffY = d3.event.sourceEvent.pageY - self.y() - self.r();
					initA = 360*self.min()/range;
				})
				.on('drag', function(d, i) {
					var dx = (d3.event.sourceEvent.pageX - self.x() - self.r());
					var dy = (d3.event.sourceEvent.pageY - self.y() - self.r());
					var a = uruburu.s_rad2deg(atan2(dy, dx)) - initA + 90;
					handler.attr('transform', 'rotate('  + a + ')');
					var v = -(a%360)/360*range;
					if (self.min() < 0) {
						if (v < self.min()) {
							self.value(self.max() - self.min() + v);
						} else {
							self.value(v, true);
						}
					} else {
						if (v < 0) {
							self.value(self.max() + v);
						} else {
							self.value(v);
						}
					}	
				});
	
			var handler = svggroup.append('svg:g')
				.style('cursor', 'pointer')
				.call(handlerDrag);
	
			handler.append('svg:circle')
				.attr('cx', 7/8*self.r()*cos(2*π+2*π*self.min()/range-π/2))
				.attr('cy', 7/8*self.r()*sin(2*π+2*π*self.min()/range-π/2))
				.attr('r', self.r()/8)
				.style('fill', 'url(#macblue)')
				.style('stroke', '#333333');
		}
		
		self.redraw = function() {
			svgroot.remove();
			self.draw();
			self.value(self.value());
		}
	
		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();
		self.value(_value);
		self.x(_x);
		self.y(_y);
	}	
	// end of PieDial






	// PieKnob
	// -----------------------------------------------------------------------------------
	uruburu.PieKnob = function (container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'color', 'x', 'y', 'r', 'levels'];
		self.type = 'PieKnob';
		
		var _name = options.name || 'pie_knob_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				self.redraw();
			} else {
				return _name;
			}
		}

		var _innerPadding = 30;
		
		self.htmlElement = container.append('div')
			.attr('id', _name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');		
		
		var _color = options.color ? options.color : '#ff0000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}
		
		var _r = options.r ? options.r : 75;
		self.r = function(val) {
			if (val !== undefined) {
				_r = val;
				self.redraw();
			} else {
				return _r;
			}
		}	
				
		self.width = function() { return 2*self.r(); };
		self.height = function() { return 2*self.r(); };

		var _levels = options.levels ? options.levels : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
		self.levels = function(val) {
			if (val !== undefined) {
				_levels = val;
				self.redraw();
			} else {
				return _levels;
			}			
		} 

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				if (!gauge || !svgtxt) return;

				gauge = gauge.data(donut(data)); // update the data
				gauge.attr('d', arcgen); // redraw the arcs
				svgtxt.text(val.toFixed());
				uruburu.emit('change', self.bind, _value);
			} else {
				return _value;
			}
		}
		
		var range, data, donut, colors, cColor;
		var arcgen, gauge, svgroot, svggroup, svgtxt, svgNametxt;
		
		self.draw = function() {
			range = self.levels().length;
			donut = d3.layout.pie().sort(null);
			
			cColor = self.color();
			colors = [];
			data = [];

			for (var i = 0; i < range; i++) {
				if (i > 0) {
					cColor = d3.rgb(cColor).brighter(0.5);
					colors.push()
				}
				colors.push(cColor);
				data.push(1);
			}
			
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', 2*self.r()+2*_innerPadding)
				.attr('height', 2*self.r()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');
			var macblueLG = defs.append('linearGradient').attr('id', 'macblue');
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#6DCAF2');
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#1a82f7');

			svgroot.append('svg:title').text('Name: ' + self.name());
			
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.r()+_innerPadding)+
					','+(self.r()+_innerPadding)+')');
			
			arcgen = d3.svg.arc().innerRadius(3*self.r()/4).outerRadius(self.r());

			gauge = svggroup.selectAll('path')
				.data(donut(data))
			  .enter().append('svg:path')
					.attr('x', self.x())
					.attr('y', self.y())
					.attr('fill', function(d, i) { return colors[i]; })
					.attr('d', arcgen)
					.each(function(d) { this._current = d; });
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', self.r()/3)
				.style('text-anchor', 'middle')
				.attr('dy', '0.25em')
				.text(0);
			
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('y', self.r() + 24)
				.attr('fill', '#999999')
				.attr('dy', '0.25em')
				.text(self.name());
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', 3*self.r()/4)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', self.r())
				.style('fill-opacity', 0)
				.style('stroke', '#333333');
			
			// ticks
			for (var i = 0; i < range; i++) {
				svggroup.append('svg:line')
					.attr('x1', 3/4*self.r()*cos(2*π+2*π*i/range-π/2))
					.attr('y1', 3/4*self.r()*sin(2*π+2*π*i/range-π/2))
					.attr('x2', (self.r()+10)*cos(2*π+2*π*i/range-π/2))
					.attr('y2', (self.r()+10)*sin(2*π+2*π*i/range-π/2))
					.style('stroke', '#333333');
			
				svggroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', 12)
					.style('text-anchor', 'middle')
					.attr('x', (self.r()+10)*cos(2*π+2*π*i/range-π/2+π/range))
					.attr('y', (self.r()+10)*sin(2*π+2*π*i/range-π/2+π/range) + 6)
					.text(i.toString());
			}
				
			var initOffX, initOffY, initA;
			var handlerDrag = d3.behavior.drag()
				.on('dragstart', function(d, i) {
					initOffX = d3.event.sourceEvent.pageX - self.x() - self.r();
					initOffY = d3.event.sourceEvent.pageY - self.y() - self.r();
				})
				.on('drag', function(d, i) {
					var dx = (d3.event.sourceEvent.pageX - self.x() - self.r());
					var dy = (d3.event.sourceEvent.pageY - self.y() - self.r());
					var a = uruburu.s_rad2deg(atan2(dy, dx)) + 90;
					a = Math.floor(a/(360/range))*360/range;
					a = a < 0 ? 360 + a : a; 
					handler.attr('transform', 'rotate('  + (a) + ')');
					var v = self.levels()[parseInt(((a + 180/range)%360)/360*range)];
					self.value(v);
				});
	
			var handler = svggroup.append('svg:g')
				.style('cursor', 'pointer')
				.call(handlerDrag);
	
			handler.append('svg:circle')
				.attr('cx', 7/8*self.r()*cos(2*π+2*π/range-π/2-π/range))
				.attr('cy', 7/8*self.r()*sin(2*π+2*π/range-π/2-π/range))
				.attr('r', self.r()/8)
				.style('fill', 'url(#macblue)')
				.style('stroke', '#333333');
		}
		
		self.redraw = function() {
			svgroot.remove();
			self.draw();
			self.value(self.value());
		}
	
		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();
		self.value(_value);
		self.x(_x);
		self.y(_y);
	}	
	// end of PieKnob







	// NumDisplay
	uruburu.NumDisplay = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'x', 'y', 'width', 'height', 'accuracy', 'percentage'];
		self.type = 'NumDisplay';
		
		var _name = options.name || 'num_display_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				self.redraw();
			} else {
				return _name;
			}
		}

		var _innerPadding = 25;
		
		self.htmlElement = container.append('div')
			.attr('id', _name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');		

		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _width = options.width ? options.width : 90;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 60;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
			
		var _percentage = options.percentage ? options.percentage : false; 
		self.percentage = function(val) { 
			if (val !== undefined) {
				_percentage = val;
				self.redraw();
			} else {
				return _percentage;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				var strVal;
				if (self.percentage()) {
					strVal = (val/range*100).toFixed(self.accuracy()) + '%';
				} else {
					strVal = val.toFixed(self.accuracy());
				}
				svgtxt.text(strVal);
				var maxVisibleChars = self.width()/self.height()*1.1;
				svgtxt.style('font-size', maxVisibleChars*(self.height()/1.4)/strVal.length);
			} else {
				return _value;
			}
		}
		
		var range, data, donut, colors, arcgen;
		var gauge, svgroot, svggroup, svgtxt, svgNametxt;
		
		self.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', 2*self.height()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');

			svgroot.append('svg:title').text('Name: ' + self.name());
		
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.width()/2+_innerPadding)+
					','+(self.height()/2+_innerPadding)+')');
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', self.height()/1.4)
				.style('text-anchor', 'middle')
				.attr('dy', '0.3em')
				.text(0);
			
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'middle')
				.attr('y', self.height())
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			svggroup.append('svg:rect')
				.attr('x', -self.width()/2)
				.attr('y', -self.height()/2)
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', 4)
				.attr('ry', 4)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:rect')
				.attr('x', -self.width()/2)
				.attr('y', -self.height()/2)
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', 4)
				.attr('ry', 4)
				.style('fill-opacity', 0)
				.style('stroke', '#333333');
		}
		
		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}
	
		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();
		self.value(_value);
		self.x(_x);
		self.y(_y);
	}
	// end of NumDisplay







	// HalfPieGauge
	uruburu.HalfPieGauge = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'min', 'max', 'color', 'x', 'y', 'r', 'accuracy', 'percentage'];
		self.type = 'HalfPieGauge';
		
		var _name = options.name || 'pie_gauge_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				self.redraw();
			} else {
				return _name;
			}
		}

		var _innerPadding = 30;
		
		self.htmlElement = container.append('div')
			.attr('id', _name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');		
		
		var _min = options.min ? options.min : 0;
		self.min = function(val) {	
			if (val !== undefined) {
				_min = val;
				self.redraw();
			} else {
				return _min;
			}	
		}

		var _max = options.max ? options.max : 100;
		self.max = function(val) {		
			if (val !== undefined) {
				_max = val;
				self.redraw();
			} else {
				return _max;
			}
		}
		
		var _color = options.color ? options.color : '#ff0000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.redraw();
			} else {
				return _color;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}
		
		var _r = options.r ? options.r : 75;
		self.r = function(val) {
			if (val !== undefined) {
				_r = val;
				self.redraw();
			} else {
				return _r;
			}
		}	
				
		self.width = function() { return 2*self.r(); };
		self.height = function() { return 2*self.r(); };

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
			
		var _percentage = options.percentage ? options.percentage : false; 
		self.percentage = function(val) { 
			if (val !== undefined) {
				_percentage = val;
				self.redraw();
			} else {
				return _percentage;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				if (val < self.min()) val = self.min();
				if (val > self.max()) val = self.max();
				_value = val;
				if (!gauge || !svgtxt) return;
				if (self.min() < 0) {
					if (val < 0) {
						data = [ self.max()/range, 
								 -val/range, 
								 (-self.min()+val)/range ];
					} else {
						data = [ (self.max()-val)/range, 
								 val/range, 
								 -self.min()/range ];
					}
				} else {
					data = [ self.min()/range, 
							 (self.max()-val-self.min())/range, 
							 (val-self.min())/range ];
				}
				gauge = gauge.data(donut(data)); // update the data
				gauge.attr('d', arcgen); // redraw the arcs
				if (self.percentage()) {
					svgtxt.text((val/range*100).toFixed(self.accuracy()) + '%');
				} else {
					svgtxt.text(val.toFixed(self.accuracy()));
				}
			} else {
				return _value;
			}
		}
		
		var range, data, donut, colors, arcgen;
		var gauge, svgroot, svggroup, svgtxt, svgNametxt;
		
		self.draw = function() {
			range = self.max() - self.min();
			donut = d3.layout.pie().sort(null);
			donut.startAngle(π/2).endAngle(-π/2);
			
			if (self.min() < 0) {
				colors = ['#aaaaaa', self.color(), '#aaaaaa'];
				data = [ self.max()/range, 
						 self.value()/range,
						 (-self.min()-self.value())/range ];
			} else {
				colors = ['#aaaaaa', '#aaaaaa', self.color()];
				data = [(self.max()-self.value()+self.min())/range, 
						(self.value()-self.min())/range,  
						self.min()/range];
			}

			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', 2*self.r()+2*_innerPadding)
				.attr('height', 2*self.r()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				blueglassLG = defs.append('linearGradient').attr('id', 'blueglass'),
				cutOffBottom = defs.append('clipPath').attr('id', 'cut-off-bottom');

			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');
			cutOffBottom.append('svg:rect')
				.attr('x', -self.r())
				.attr('y', -self.r())
				.attr('width', self.r()*2)
				.attr('height', self.r());

			svgroot.append('svg:title').text('Name: ' + self.name());
			
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.r()+_innerPadding)+
					','+(self.r()+_innerPadding)+')');
			
			arcgen = d3.svg.arc().innerRadius(3*self.r()/4).outerRadius(self.r());
			
			gauge = svggroup.selectAll('path')
				.data(donut(data))
			  .enter().append('svg:path')
				.attr('x', self.x())
				.attr('y', self.y())
				.attr('fill', function(d, i) { return colors[i]; })
				.attr('d', arcgen)
				.each(function(d) { this._current = d; });
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', self.r()/3)
				.style('text-anchor', 'middle')
				.attr('y', -self.r()/3)
				.attr('dy', '0.25em')
				.text(0);
			
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'middle')
				.attr('y', 20)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', 3*self.r()/4)
				.attr('clip-path', 'url(#cut-off-bottom)')
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:circle')
				.attr('cx', 0)
				.attr('cy', 0)
				.attr('r', self.r())
				.attr('clip-path', 'url(#cut-off-bottom)')
				.style('fill-opacity', 0)
				.style('stroke', '#333333');
			
			// bottom line
			svggroup.append('svg:line')
				.attr('x1', -self.r())
				.attr('y1', 0)
				.attr('x2', self.r())
				.attr('y2', 0)
				.style('stroke', '#333333');

			// min/max tick
			svggroup.append('svg:line')
				.attr('x1', self.r())
				.attr('y1', 0)
				.attr('x2', self.r()+20)
				.attr('y2', 0)
				.style('stroke', '#333333');

			svggroup.append('svg:line')
				.attr('x1', -self.r())
				.attr('y1', 0)
				.attr('x2', -self.r()-20)
				.attr('y2', 0)
				.style('stroke', '#333333');
		
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'start')
				.attr('x', self.r() + 4)
				.attr('y', -2)
				.text(self.max().toString());
			
			// min text
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'end')
				.attr('x', -self.r()-4)
				.attr('y', -2)
				.text(self.min().toString());
			
			if (self.min() < 0) {
				svggroup.append('svg:line')
					.attr('x1', 3/4*self.r()*cos(2*π+π*self.min()/range-π/3))
					.attr('y1', 3/4*self.r()*sin(2*π+π*self.min()/range-π/3))
					.attr('x2', (self.r()+10)*cos(2*π+π*self.min()/range-π/3))
					.attr('y2', (self.r()+10)*sin(2*π+π*self.min()/range-π/3))
					.style('stroke', '#333333');
				
				var svg0txt = svggroup.append('svg:text')
					.attr('y', (self.r()+12)*sin(2*π+π*self.min()/range-π/3))
					.style('text-anchor', 'start') 
					.style('font-family', 'sans-serif')
					.style('font-size', '1em')
					.attr('dx', 6)
					.text(0);
				
				if (Math.abs(2*π+2*π*self.min()/range-π/2) > π/2) {
					svg0txt.style('text-anchor', 'end')
						.attr('x', (self.r()+12)*cos(2*π+π*self.min()/range-π/3) - 2)
						.attr('y', (self.r()+12)*sin(2*π+π*self.min()/range-π/3));
				} else if (Math.abs(2*π+2*π*self.min()/range-π/2) < π/2) {
					svg0txt.style('text-anchor', 'start')
						.attr('x', (self.r()+12)*cos(2*π+π*self.min()/range-π/3) + 2)
						.attr('y', (self.r()+12)*sin(2*π+π*self.min()/range-π/3));
				} else {
					svg0txt.style('text-anchor', 'middle');
				}
			}		
		}
		
		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}
	
		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();
		self.value(_value);
		self.x(_x);
		self.y(_y);
	}
	// end of HalfPieGauge







	// NumStepper
	uruburu.NumStepper = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'x', 'y', 'width', 'height', 'step', 'accuracy'];
		self.type = 'NumStepper';
		
		var _name = options.name || 'num_stepper_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				self.redraw();
			} else {
				return _name;
			}
		}

		var _innerPadding = 25;
		
		self.htmlElement = container.append('div')
			.attr('id', _name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');		

		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _width = options.width ? options.width : 90;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 60;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 

		var _step = options.step ? options.step : 1;
		self.step = function(val) {
			if (val !== undefined) {
				_step = val;
			} else {
				return _step;
			}			
		} 

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				_value = val;
				var strVal = val.toFixed(self.accuracy());
				svgtxt.text(strVal);
				var maxVisibleChars = self.width()/self.height()*1.3;
				svgtxt.style('font-size', self.height()/1.3 * maxVisibleChars/strVal.length );
				uruburu.emit('change', self.bind, _value);
			} else {
				return _value;
			}
		}
		
		var range, data, donut, colors, arcgen;
		var gauge, svgroot, svggroup, svgtxt, svgNametxt;
		
		self.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', 2*self.height()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				blueglassLG = defs.append('linearGradient').attr('id', 'blueglass');
			blueglassLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#666688');
			blueglassLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#9999ff');
		
			var macgreenLG = defs.append('linearGradient').attr('id', 'macgreen');
			macgreenLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#5D9426');
			macgreenLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#6CD900');
			
			var macredLG = defs.append('linearGradient').attr('id', 'macred');
			macredLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#F00A0A');
			macredLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#FF5252');	

			svgroot.append('svg:title').text('Name: ' + self.name());
		
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(self.width()/2+_innerPadding)+
					','+(self.height()/2+_innerPadding)+')');
			
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', self.height()/1.3)
				.style('text-anchor', 'middle')
				.attr('dy', '0.35em')
				.text(0);
			
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', '1em')
				.style('text-anchor', 'middle')
				.attr('y', self.height())
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			svggroup.append('svg:rect')
				.attr('x', -self.width()/2)
				.attr('y', -self.height()/2)
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', 4)
				.attr('ry', 4)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:rect')
				.attr('x', -self.width()/2)
				.attr('y', -self.height()/2)
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', 4)
				.attr('ry', 4)
				.style('fill-opacity', 0)
				.style('stroke', '#333333');

			// plus button
			svggroup.append('svg:circle')
				.attr('cx', -self.width()/2-10)
				.attr('cy', -self.height()/2-5)
				.attr('r', 10)
				.style('fill', 'url(#macgreen)')
				.style('stroke', '#333333')
				.style('cursor', 'pointer')
				.on('click', function() {
					d3.event.stopPropagation();
					d3.event.preventDefault();
					self.value(self.value()+self.step());
				});	
			
			// minus button
			svggroup.append('svg:circle')
				.attr('cx', -self.width()/2-10)
				.attr('cy', +self.height()/2+5)
				.attr('r', 10)
				.style('fill', 'url(#macred)')
				.style('stroke', '#333333')
				.style('cursor', 'pointer')
				.on('click', function() {			
					d3.event.stopPropagation();
					d3.event.preventDefault();
					self.value(self.value()-self.step());
				});	
		}
		
		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}
	
		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();
		self.value(_value);
		self.x(_x);
		self.y(_y);
	}
	// end of NumStepper








	// PushButton
	uruburu.PushButton = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'x', 'y', 'width', 'height'];
		self.type = 'PushButton';
		
		var _name = options.name || 'push_button_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = 35;
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
	
		var _width = options.width ? options.width : 60;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 30;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {	
			if (val !== undefined) {
				_value = val;
				uruburu.emit('change', self.bind, _value);
			} else {
				if (_value) {
					_value = 0;
					return 1;
				}
				return _value;
			}
		}

		var svgroot, svggroup, svgtxt, svgNametxt, button;
		
		self.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);
				
			var defs = svgroot.append('svg:defs'),
				macblueLG = defs.append('linearGradient').attr('id', 'macblue');
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#6DCAF2');
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#1a82f7');

			svgroot.append('svg:title').text('Name: ' + self.name());
					
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(_innerPadding)+
					','+(_innerPadding)+')');
	
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', 15)
				.style('fill', 'white')
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', 15)
				.style('fill', 'white')
				.style('stroke-opacity', 0);
			
			button = svggroup.append('svg:rect')
				.attr('x', 1)
				.attr('y', 1)
				.attr('width',  self.width()-2)
				.attr('height', self.height()-2)
				.attr('rx', 15)
				.style('fill', 'url(#macblue)')
				.style('stroke', '#333333')
				.style('cursor', 'pointer')
				.on('mousedown', function() {
					self.value(1);
					d3.select(this).style('stroke-width', 3);
				})
				.on('mouseup', function() {
					d3.select(this).style('stroke-width', 1);
				});
		
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', self.width()/2)
				.attr('y', self.height() + 10)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
		}

		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}

		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();		
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
		self.value(_value);
	}
	// end of PushButton






	// FunctionButton
	uruburu.FunctionButton = function(container,options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'x', 'y'];
		self.type = 'FunctionButton';
		
		var _name = options.name || 'function_button_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = 35;
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
	
		var _width = options.width ? options.width : 60;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 30;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
		
		var _color = options.color ? options.color : ['#6DCAF2', '#1a82f7'];
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				macblueLG.remove();
				macblueLG = defs.append('linearGradient')
					.attr('id', 'macblue' + self.color()[0].replace('#','') + self.color()[1].replace('#',''));
				macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', self.color()[0]);
				macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', self.color()[1]);
				self.redraw();
			} else {
				return _color;
			}
		}

		var _callback = options.callback ? options.callback : undefined;
		self.callback = function(val) {		
			if (val !== undefined) {
				_callback = val;
			} else {
				return _callback;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var svgroot, svggroup, svgtxt, svgNametxt, button, defs, macblueLG;
		
		self.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);
				
			defs = svgroot.append('svg:defs');
			macblueLG = defs.append('linearGradient')
				.attr('id', 'macblue' + self.color()[0].replace('#','') + self.color()[1].replace('#',''));
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', self.color()[0]);
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', self.color()[1]);

			svgroot.append('svg:title').text('Name: ' + self.name());
					
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(_innerPadding)+
					','+(_innerPadding)+')');
	
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', 8)
				.style('fill', 'white')
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			button = svggroup.append('svg:rect')
				.attr('x', 1)
				.attr('y', 1)
				.attr('width',  self.width()-2)
				.attr('height', self.height()-2)
				.attr('rx', 8)
				.style('fill', 'url(#'+ 'macblue' + self.color()[0].replace('#','') + self.color()[1].replace('#','') +')')
				.style('stroke', '#333333')
				.style('cursor', 'pointer')
				.on('mousedown', function() {
					d3.select(this).style('stroke-width', 3);
					callbacks[self.callback()]();
				})
				.on('mouseup', function() {
					d3.select(this).style('stroke-width', 1);
				});
		
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', self.width()/2)
				.attr('y', self.height() + 10)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
		}

		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}

		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();		
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
		self.color(_color);
	}
	// end of FunctionButton








	// SwitchButton
	uruburu.SwitchButton = function(container,options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'x', 'y', 'width', 'height'];
		self.type = 'SwitchButton';
		
		var _name = options.name || 'switch_button_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = 35;
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
	
		var _width = options.width ? options.width : 60;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 30;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {	
			if (val !== undefined) {
				self.update(val);
				uruburu.emit('change', self.bind, _value);
			} else {
				return _value;
			}
		}

		self.update = function(val) {
			_value = val;
			svgtxt.text(_value ? 'ON': 'OFF');
			svgtxt.attr('transform', 'translate(' + (_value ? 14 : self.width()/2 +15) + ',0)');
			button.attr('transform', 'translate(' + (_value ? self.width()/2 : 0) + ',0)');	
		}

		var svgroot, svggroup, svgtxt, svgNametxt, button;
		
		self.draw = function() {
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);
				
			var defs = svgroot.append('svg:defs'),
				macblueLG = defs.append('linearGradient').attr('id', 'macblue'),
				macdarkLG = defs.append('linearGradient').attr('id', 'macdark');
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#6DCAF2');
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#1a82f7');
			macdarkLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#45484d');
			macdarkLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#000000');

			svgroot.append('svg:title').text('Name: ' + self.name());
					
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(_innerPadding)+
					','+(_innerPadding)+')');
	
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', self.height()/2)
				.style('fill', 'white')
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
			
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', self.height()/8)
				.style('fill', 'white')
				.style('stroke-opacity', 0);
		
			svgtxt = svggroup.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', 11)
				.style('text-anchor', 'middle')
				.attr('y', self.height()/2)
				.attr('dy', '0.25em')
				.text('OFF');
			
			svggroup.append('svg:rect')
				.attr('width', self.width())
				.attr('height', self.height())
				.attr('rx', self.height()/8)
				.style('fill', 'url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333');	
		
			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.attr('x', self.width()/2)
				.attr('y', self.height() + 10)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			button = svggroup.append('svg:g')
				.on('click', function() {
					self.value(!self.value());
				});

			button.append('svg:rect')
				.attr('width', self.height())
				.attr('height', self.height())
				.attr('rx', self.height()/8)
				.style('fill', 'url(#macdark)')
				.style('stroke', '#333333')
				.style('cursor', 'pointer');

			button.append('svg:rect')
				.attr('width', self.height()/2)
				.attr('height', self.height())
				.attr('rx', self.height()/8)
				.style('fill', 'url(#macblue)')
				.style('stroke', '#333333')
				.style('cursor', 'pointer');
		}

		self.redraw = function() {
			svgroot.remove();
			self.draw();
		}

		self.remove = function() {
			self.htmlElement.remove();
		}
		
		self.draw();		
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
		self.value(_value);
	}
	// end of SwitchButton









	// ThrottleControl
	uruburu.ThrottleControl = function(container, options) {
		var self = this;

		container = d3.select(container);
		if (container.empty()) console.log('empty');
		options = options || {};

		self.editables = ['name', 'min', 'max', 'color', 'x', 'y', 
			'width', 'height', 'accuracy', 'percentage'];

		self.type = 'ThrottleControl';
		
		var _name = options.name || 'throttle_control_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
				svgNametxt.text(value);
			} else {
				return _name;
			}
		}
			
		var _innerPadding = 50;
		
		this.htmlElement = container.append('div')
			.attr('id', name)
			.style('cursor', 'pointer')
			.style('pointer-events', 'auto')
			.style('position', 'absolute');
	
		var _width = options.width ? options.width : 40;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;
				self.redraw();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : 160;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.redraw();
			} else {
				return _height;
			}
		}
	
		var _min = options.min ? options.min : 0;
		self.min = function(val) {	
			if (val !== undefined) {
				_min = val;
				self.redraw();
			} else {
				return _min;
			}	
		}

		var _max = options.max ? options.max : 100;
		self.max = function(val) {		
			if (val !== undefined) {
				_max = val;
				self.redraw();
			} else {
				return _max;
			}
		}
		
		var _upColor = options.upColor ? options.upColor : 'lightgreen';
		self.upColor = function(val) {		
			if (val !== undefined) {
				_upColor = val;
				self.redraw();
			} else {
				return _upColor;
			}
		}
		
		var _downColor = options.downColor ? options.downColor : 'red';
		self.downColor = function(val) {		
			if (val !== undefined) {
				_downColor = val;
				self.redraw();
			} else {
				return _downColor;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _accuracy = options.accuracy ? options.accuracy : 0;
		self.accuracy = function(val) {
			if (val !== undefined) {
				_accuracy = val;
				self.redraw();
			} else {
				return _accuracy;
			}			
		} 
			
		var _percentage = options.percentage ? options.percentage : false; 
		self.percentage = function(val) { 
			if (val !== undefined) {
				_percentage = val;
				self.redraw();
			} else {
				return _percentage;
			}
		}	
				
		var _value = options.initial ? options.initial : 0;
		self.value = function(val) {
			if (val !== undefined) {
				self.update(val);
				uruburu.emit('change', self.bind, _value);
			} else {
				return _value;
			}
		}

		self.update = function(val) {
			_value = val;
			var refY = self.height()+self.min()*self.height()/range;

			if (!gauge || !svgtxt || isNaN(val)) return;

			if (val >= 0) {
				gauge.attr('y', refY - val*self.height()/range)
					.attr('height', val*self.height()/range)
					.style('fill', self.upColor());
			} else {
				gauge.attr('y', refY)
					.attr('height', -val*self.height()/range)
					.style('fill', self.downColor());
			}
			
			var str;
			if (self.percentage()) {
				str = (val/range*100).toFixed(0) + '%';
			} else {
				str = val.toFixed(self.accuracy());
			}
			svgtxt.style('font-size', Math.min(12, 12*3/str.length)).text(str);	

			handler.attr('transform', 'translate(0,'  + (self.height()-(val-self.min())/range*self.height()) + ')');
		}

		var range, frameRadius, fontSize, 
			svgroot, gauge, handler, svgtxt, svggroup, svgNametxt;
	
		self.draw = function() {
			_innerPadding = self.width() + 16;
			range = self.max() - self.min();
	
			svgroot = self.htmlElement.append('svg')
				.style('left', (-_innerPadding)+'px')
				.style('top', (-_innerPadding)+'px')
				.style('position', 'absolute')
				.attr('width', self.width()+2*_innerPadding)
				.attr('height', self.height()+2*_innerPadding);

			var defs = svgroot.append('svg:defs'),
				macblueLG = defs.append('linearGradient').attr('id', 'macblue');
			macblueLG.append('svg:stop').attr('offset', '0%').style('stop-color', '#6DCAF2');
			macblueLG.append('svg:stop').attr('offset', '100%').style('stop-color', '#1a82f7');

			svgroot.append('svg:title').text('Name: ' + self.name());
					
			svggroup = svgroot.append('svg:g')
				.attr('transform', 'translate('+(_innerPadding)+
					','+(_innerPadding)+')');

			svgNametxt = svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'start')
				.attr('x', 0)
				.attr('y', self.height() + self.width()/2 + 6)
				.attr('dy', '0.25em')
				.attr('fill', '#999999')
				.text(self.name());
			
			// track
			svggroup.append('svg:rect')
				.attr('x', 0)
				.attr('y', 0)
				.attr('width', self.width())
				.attr('height', self.height())
				.style('fill','url(#blueglass)')
				.style('fill-opacity', 0.3)
				.style('stroke', '#333333')
				.style('stroke-width', 2);
			
			frameRadius = self.width()/2;
			fontSize = self.width()/2;
			
			// min tick
			svggroup.append('svg:line')
				.attr('x1', self.width()/2-10)
				.attr('y1', self.height())
				.attr('x2', self.width()+10)
				.attr('y2', self.height())
				.style('stroke', '#333333');
		
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'start')
				.attr('x', self.width()+12)
				.attr('y', self.height())
				.attr('dy', '0.25em')
				.text(self.min().toString());
			
			// max tick
			svggroup.append('svg:line')
				.attr('x1', -10)
				.attr('y1', 0)
				.attr('x2', self.width()/2+10)
				.attr('y2', 0)
				.style('stroke', '#333333');
			
			svggroup.append('svg:text')
				.style('font-family', 'sans-serif')
				.style('font-size', 12)
				.style('text-anchor', 'end')
				.attr('x', -12)
				.attr('y', 0)
				.attr('dy', '0.25em')
				.text(self.max().toString());
			
			if (self.min() < 0) {
				svggroup.append('svg:line')
					.attr('x1', -10)
					.attr('y1', self.height()+self.min()*self.height()/range)
					.attr('x2', self.width())
					.attr('y2', self.height()+self.min()*self.height()/range)
					.style('stroke', '#333333');
				
				svggroup.append('svg:text')
					.style('font-family', 'sans-serif')
					.style('font-size', 12)
					.style('text-anchor', 'end')
					.attr('x', -12)
					.attr('y', self.height()+self.min()*self.height()/range)
					.attr('dy', '0.25em')
					.text(0);
			}
			
			var refY = self.height()+self.min()*self.height()/range;

			gauge = svggroup.append('svg:rect')
				.attr('stroke-opacity', 0)
				.attr('stroke-width', 3)
				.attr('x', 1)
				.attr('y', refY)
				.attr('width', self.width()-2)
				.attr('height', 0)
				.style('fill',self.upColor())
				.style('stroke-opacity', 0);

			handler = svggroup.append('svg:g')
				.attr('transform', 'translate(0, ' + refY + ')')
				.style('cursor', 'pointer')
				.style('pointer-events', 'auto');

			var initOffX, initOffY, posX, posY;
			var handlerDrag = d3.behavior.drag()
				.on('dragstart', function(d, i) {
					initOffY = d3.event.sourceEvent.pageY;
					if (handler.attr('transform')) {
						posY = parseInt(handler
								.attr('transform')
								.replace('translate(','')
								.replace(')','')
								.split(',')[1]) - refY;
					}
				})
				.on('drag', function(d, i) {
					var newPosY = posY + (d3.event.sourceEvent.pageY - initOffY);
					if (newPosY >= -self.max()/range*self.height() && 
							newPosY <= -self.min()/range*self.height()) {
						self.value(-newPosY*range/self.height());
					} else {
						if (newPosY < -self.max()/range*self.height()) {
							self.value(self.max());
						}
						if (newPosY > -self.min()/range*self.height()) {
							self.value(self.min());
						}
					}
				});
			
			handler.call(handlerDrag);	
			
			handler.append('svg:rect')
				.attr('x', -8)
				.attr('y', -13)
				.attr('width', 2*self.width()+16)
				.attr('height', 26)
				.attr('rx', 4)
				.style('fill', 'url(#macblue)')
				.style('stroke', '#333333');
			
			svgtxt = handler.append('svg:text')
				.style('font-family', 'DigitaldreamRegular')
				.style('font-size', 12)
				.style('text-anchor', 'middle')
				.style('fill', 'white')
				.attr('x', self.width()/2)
				.attr('y', 0)
				.attr('dy', '0.25em')
				.text(0);
		}

		self.redraw = function() {
			svgroot.remove();
			self.draw();
			self.value(self.value());
		}
		
		self.remove = function() {
			self.htmlElement.remove();
		}
	
		self.draw();		
		self.value(_value);
		self.x(_x);
		self.y(_y);			
		self.width(_width);
		self.height(_height);	
	}	
	// end of ThrottleControl










	// RTPlot
	uruburu.RTPlot = function(container, options) {
		var self = this;
		options = options || {};

		self._color = options.color || 'black';

		self.htmlElement = d3.select(container).append('div')
			.attr('id', this._name)
			.style('position', 'absolute')
			.style('height', this._height + 'px')
			.style('width', this._width + 'px')
			.style('top', this._y + 'px')
			.style('left', this._x + 'px');

		self.editables = ["xLabel", "yLabel", "yRangeMin", "yRangeMax", "graduationsVisible",
			"plotColor", "yGraduationsDelta", "secPerPixel", "timePerDivision"];
		self.type = 'RTPlot';
		
		var _name = options.name || 'rt_plot_' + uruburu.randStr();
		self.name = function(val) {
			if (val !== undefined) {
 				_name = val;
			} else {
				return _name;
			}
		}
	
		var _x = options.x ? options.x : 0;
		self.x = function(val) {
			if (val !== undefined) {
				_x = val; 
				self.htmlElement.style('left', val + 'px'); 
			} else {
				return _x;
			}
		}

		var _y = options.y ? options.y : 0;
		self.y = function(val) {
			if (val !== undefined) {
				_y = val; 
				self.htmlElement.style('top', val + 'px'); 
			} else {
				return _y;
			}
		}

		var _width = options.width ? options.width : window.innerWidth;
		self.width = function(val) {
			if (val !== undefined) {
				_width = val;				
				self.htmlElement.style('width', val + 'px');
				_gridArea.width(val);
				_graduationsArea.width(val);
				_drawingArea.width(val);
				self.reinitialize();
			} else {
				return _width;
			}
		}
		
		var _height = options.height ? options.height : window.innerHeight;
		self.height = function(val) {
			if (val !== undefined) {
				_height = val;
				self.htmlElement.style('height', val + 'px');
				_gridArea.height(val);
				_graduationsArea.height(val);
				_drawingArea.height(val);
				this.reinitialize();
			} else {
				return _height;
			}
		}
		
		var _color = options.color ? options.color : '#000000';
		self.color = function(val) {		
			if (val !== undefined) {
				_color = val;
				self.reinitialize();
			} else {
				return _color;
			}
		}

		var _xLabel = options.xLabel ? options.xLabel : 'Time (s)';
		self.xLabel = function(val) {	
			if (val !== undefined) {
				_xLabel = val;
				self.reinitialize();
			} else {
				return _xLabel;
			}	
		}

		var _yLabel = options.yLabel ? options.yLabel : 'Voltage (V)';
		self.yLabel = function(val) {	
			if (val !== undefined) {
				_yLabel = val;
				self.reinitialize();
			} else {
				return _yLabel;
			}	
		}

		var _timePerDivision = options.timePerDivision ? options.timePerDivision : 0.001;
		self.timePerDivision = function(val) {	
			if (val !== undefined) {
				_timePerDivision = val;
				self.reinitialize();
			} else {
				return _timePerDivision;
			}	
		}

		var _tRange = options.tRange ? options.tRange : [0, 100];
		self.tRange = function(val) {	
			if (val !== undefined) {
				console.log(val)
				_tRange = val;
				self.reinitialize();
			} else {
				return _tRange;
			}	
		}

		var _yRangeMin = options.yRangeMin != undefined ? options.yRangeMin : -50;
		self.yRangeMin = function(val) {	
			if (val !== undefined) {
				_yRangeMin = val;
				_yRange = [val, this._yRangeMax];
				self.reinitialize();
			} else {
				return _yRangeMin;
			}	
		}

		var _yRangeMax = options.yRangeMax != undefined ? options.yRangeMax : 50;
		self.yRangeMin = function(val) {	
			if (val !== undefined) {
				_yRangeMax = val;
				_yRange = [this._yRangeMin, val];
				self.reinitialize();
			} else {
				return _yRangeMax;
			}	
		}

		var _yRange = [_yRangeMin, _yRangeMax];

		var _secPerPixel = options.secPerPixel ? options.secPerPixel : 0.00001;
		self.secPerPixel = function(val) {	
			if (val !== undefined) {
				_secPerPixel = val;
				self.reinitialize();
			} else {
				return _secPerPixel;
			}	
		}

		var _yGraduationsDelta = options.yGraduationsDelta ? options.yGraduationsDelta : 10;
		self.yGraduationsDelta = function(val) {	
			if (val !== undefined) {
				if (_yGraduationsDelta*_drawingArea.height()/_yAxisRange() >= 10) { 
					_yGraduationsDelta = val;
					self.reinitialize();
				}
			} else {
				return _yGraduationsDelta;
			}	
		}

		var _graduationsVisible = options.graduationsVisible != undefined ? options.graduationsVisible : true;
		self.graduationsVisible = function(val) {	
			if (val !== undefined) {
				_graduationsVisible = val;
				self.xGrid(val);
				self.yGrid(val);
				self.drawGraduations();
			} else {
				return _graduationsVisible;
			}	
		}
		
		var _xGrid = options.xGrid != undefined ? options.xGrid : true;
		self.xGrid = function(val) {	
			if (val !== undefined) {
				_xGrid = val;
				self.drawGrid();
			} else {
				return _xGrid;
			}	
		}

		var _yGrid = options.yGrid != undefined ? options.yGrid : true;
		self.yGrid = function(val) {	
			if (val !== undefined) {
				_yGrid = val;
				self.drawGrid();
			} else {
				return _yGrid;
			}	
		}

		var _gridArea = new Area(self, { name: 'grid' }),
			_graduationsArea = new Area(self, { name: 'graduations' }),
			_drawingArea = new Area(self, { name: 'drawing' }),
			_startTime =  0.0,
			_path = [],
			_primGradHeight = 6,
			_drawing = false,
			_yValue = 0.0,
			_t = 0.0,
			_zeroY = 0.0,
			_initY = 0.0,
			cycleTimestamp = 0.0,
			lastUpdateTimestamp = 0,
			_tAxisRange = function() {
				return _tRange[1] - _tRange[0];
			},
			_yAxisRange = function() {
				return _yRange[1] - _yRange[0];
			};

		var drag = d3.behavior.drag()
  		.on("drag", self.yReferenceHook_dragMove);

		var yReferenceHook = self.htmlElement.append('div')
			.style('background-color', this._color)
			.style('position', 'absolute')
			.style('top', this._zeroY)
			.style('right', '1px')
			.call(drag);

		function Area(parent, options) {
			options = options || {};
			this._width = options.width || parent.width();
			this._height = options.height || parent.height();

			var canvas = parent.htmlElement.append('canvas')
				.attr('id', options.name)
				.style('position', 'absolute')
				.attr('width', this._width)
				.attr('height', this._height);

			this.graphics = canvas[0][0].getContext('2d');

			this.width = function(val) {
				if (val != undefined) {
					this._width = val;
					canvas.attr('width', val ); 
				} else {
					return this._width;
				}
			}

			this.height = function(val) {
				if (val != undefined) {
					this._height = val;
					canvas.attr('height', val);
				} else {
					return this._height;
				}
			}

			this.clear = function() {
				this.graphics.clearRect(0, 0, this._width, this._height);
			}

			this.lineStyle = function(width, color) {
				this.graphics.lineWidth = width;
				this.graphics.strokeStyle = color;
			}

			return this;
		}

		var _value = [];
		self.value = function(val) {
			if (val != undefined) {
				if (val != null) {
					_value = [];
					_value.push(val[0]);
					_t = val[0]/1000000;
					_value.push(val[1]);
					_yValue = val[1];
					if (!_drawing) {
						_drawing = true;
						_startTime = val[0];
					}
				} else {
					_value = null;
				}
				self.update();
			} else {
				return _value;
			}
		},

		self.real2local = function(p) {
			var gp = { x: 0, y: 0 };
			gp.x = (p.x - _tRange[0]) / _secPerPixel;
			gp.y = _drawingArea.height() - (p.y - _yRange[0]) * _drawingArea.height()/_yAxisRange();
			return gp;
		},

		self.local2real = function(p) {
			var lp = { x: 0, y: 0 };
			lp.x = p.x * _secPerPixel - _tRange[0];
			lp.y = (-p.y + _drawingArea.height()) * _yAxisRange() / _drawingArea.height() + _yRange[0];
			return lp;
		},

		self.drawGraduations = function() {
			_graduationsArea.clear();
			if (_graduationsVisible) {
				// current graduation for t axis
				var tGradValue = _tRange[0] + _timePerDivision;
				// current graduation for y axis
				var yGradValue = _yRange[0] + _yGraduationsDelta;
				
				// label display
				var label;
				// point to be used temporary in computations
				var p;
				
				_graduationsArea.lineStyle(2, _color, 0.9);

				// position on t axis for graduations and labels
				var yPos = self.height() - 16 - _primGradHeight;
				// draws t graduations 
				for (var xR = tGradValue; xR < _tRange[1]; xR += _timePerDivision) {
					p = self.real2local({ x: xR, y: 0});
				
					_graduationsArea.graphics.beginPath();
					_graduationsArea.graphics.moveTo(p.x, yPos);
					_graduationsArea.graphics.lineTo(p.x, yPos - _primGradHeight);
					_graduationsArea.graphics.stroke();

					tGradValue += _timePerDivision;
				}
				
				// position on x axis for graduations and labels
				var xPos = 0;
				// draws y graduations 
				for (var yR = yGradValue; yR < _yRange[1]; yR += _yGraduationsDelta) {
					p = self.real2local({ x: 0, y: yR});

					_graduationsArea.graphics.beginPath();
					_graduationsArea.graphics.moveTo(xPos, p.y);
					_graduationsArea.graphics.lineTo(xPos + _primGradHeight, p.y);
					_graduationsArea.graphics.stroke();
					
					_graduationsArea.graphics.textAlign = 'left';
      		_graduationsArea.graphics.textBaseline = 'bottom';
		  		_graduationsArea.graphics.font = "16px Courrier New";
		  		_graduationsArea.graphics.fillStyle = '#666';
					_graduationsArea.graphics.fillText(yR, xPos + _primGradHeight + 2, p.y);

					yGradValue += _yGraduationsDelta;
				}	
			}

			_graduationsArea.graphics.save();

		  _graduationsArea.graphics.fillStyle = _color;
      _graduationsArea.graphics.rect(0, self.height() - 20, self.width(), self.height());
      _graduationsArea.graphics.rect(self.width() - 20, 0, self.width(), self.height());
      _graduationsArea.graphics.fill();

      _graduationsArea.graphics.textAlign = 'center';
      _graduationsArea.graphics.textBaseline = 'bottom';
		  _graduationsArea.graphics.font = "16px Courrier New";
		  _graduationsArea.graphics.fillStyle = '#fff';
			_graduationsArea.graphics.fillText(self.xLabel(), self.width()/2, self.height());

			_graduationsArea.graphics.translate(self.width() - 2, self.height()/2)
			_graduationsArea.graphics.rotate(-Math.PI/2);
			_graduationsArea.graphics.fillText(self.yLabel(), 0, 0);

			_graduationsArea.graphics.restore();
		}

		self.drawGrid = function() {
			_gridArea.clear();
			
			var label, p;
			
			_gridArea.lineStyle(1, 'rgba(0, 0, 0, 0.8)');
			var yPos = _drawingArea.height();
			if (_xGrid)
				for (var xR = _tRange[0]; xR <= _tRange[1]; xR += _timePerDivision) {
					p = self.real2local({ x: xR, y: 0});
					_gridArea.graphics.beginPath();
					_gridArea.graphics.moveTo(p.x, yPos);
					_gridArea.graphics.lineTo(p.x, 0);
					_gridArea.graphics.stroke();
				}

			var xPos = 0;
			if (_yGrid) {
				for (var yR = _yRange[0]; yR <= _yRange[1]; yR += _yGraduationsDelta) {
					p = self.real2local({ x: 0, y: yR });
					_gridArea.graphics.beginPath();
					_gridArea.graphics.moveTo(xPos, p.y);
					_gridArea.graphics.lineTo(_drawingArea.width(), p.y);
					_gridArea.graphics.stroke();
				}		
			} 
		}

		self.reinitialize = function() {
			// updates the startTime
			_startTime = _t;
			_tRange[0] = 0;
			
			// bug handling
			// detected on update: no other solution than forcing here the update
			_drawingArea.width(_width-20);
			_drawingArea.height(_height-20);
			_drawingArea.clear();
			// end of bug handling
			
			// update the tRange used for graduations
			_tRange[1] = _tRange[0] + _drawingArea.width() * _secPerPixel;

			// redraw graduations
			self.drawGraduations();
			self.drawGrid();
			self.clearPath();
			if (_drawing) {
				var p = { x: 0, y: 0 }, rp;
				p.y = _yValue;
				p.x = 0;
				rp = self.real2local(p);
				_path.push(rp);
				self.drawPath(_path);
			}
			
			_zeroY = self.real2local({ x: 0, y: 0 }).y;
		}
			
		self.drawPath = function(data) {
			if (data.length == 0) return;
			_drawingArea.clear();

			_drawingArea.lineStyle(2, _color);
			_drawingArea.graphics.beginPath();
			for (var i = 0; i < data.length; i++) {
				var p = data[i];
				if (i == 0) {
					_drawingArea.graphics.moveTo(p.x, p.y);
				} else {
					_drawingArea.graphics.lineTo(p.x, p.y);
				}
			}
			_drawingArea.graphics.stroke();
		}

		self.clearPath = function() {
			_drawingArea.graphics.clearRect(0, 0, _width, _height);
			_path = [];
		}

		self.clearGraduations = function() {
			_graduationsArea.clear();
		}

 		self.slidePath = function(data, offset) {
			var result = [];
			for (var i = 0; i < data.length; i++) {
				var np = { x: data[i].x - offset, y: data[i].y };
				if (np.x > 1) result.push(np);
			}
			return result;
		}

		self.update = function() { 
			if (_drawingArea) {
				var p = { x: 0, y: 0 }, rp;
				p.y = _yValue;
				if (_drawing) {
					p.x = _t - _startTime;
					rp = self.real2local(p);
					rp.x = _drawingArea.width() - 1;
					_path = self.slidePath(_path, p.x / _secPerPixel);
					_path.push(rp);
					self.drawPath(_path);
					_startTime = _t;
				}
			}
		}

		self.resize = function(w, h) {
			self.width(w);
			self.height(h);
		}

		self.yReferenceHook_dragMove = function() {
			/**event.stopImmediatePropagation();
			initY = event.stageY;
			yReferenceHook.startDrag(true, new Rectangle(width-2,0,0,height-20));
			systemManager.addEventListener(MouseEvent.MOUSE_UP, yReferenceHook_mouseUpHandler);*/
		}

		self.yReferenceHook_mouseDownHandler = function(event) {
			/**event.stopImmediatePropagation();
			initY = event.stageY;
			yReferenceHook.startDrag(true, new Rectangle(width-2,0,0,height-20));
			systemManager.addEventListener(MouseEvent.MOUSE_UP, yReferenceHook_mouseUpHandler);*/
		},

		self.yReferenceHook_mouseUpHandler = function(event) {
			event.stopImmediatePropagation();
			yReferenceHook.stopDrag();
			
			var deltaY = event.stageY - this._initY;
			var realDeltaY = deltaY * this.yAxisRange() / _drawingArea.height;
			
			this._yRange = [this._yRange[0] + realDeltaY, this._yRange[1] + realDeltaY];
			
			systemManager.removeEventListener(MouseEvent.MOUSE_UP, yReferenceHook_mouseUpHandler);
		}	

		self.reinitialize();
		self.xGrid(self.xGrid());
		self.yGrid(self.yGrid());

	}
	// end of RTPlot

	return uruburu;
});