# uruburu


## UI Widgets for real time data

Based on Web components concept and Polymer implementation, this microlibrary provides several components for automation applications. These are widgets that allow you to watch or update real time data.

Library can be used as Javascript one as well, without Polymer. 

Tests have been done using Chrome. Touch screens are not currently managed in a proprer way, but library works on Android KitKat devices.

![Screen capture](https://github.com/vandrito/uruburu/blob/master/assets/uruburu.png)

## Install

    bower install uruburu
    
or clone this repository to your disk.
  
## Usage

Working example is provided [here](https://github.com/vandrito/uruburu_mqtt)

